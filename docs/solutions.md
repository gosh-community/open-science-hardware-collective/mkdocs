# Solutions

### For Users
- Provide users with consistent, compartable, high quality product
- Provide users with the ability to easily purchase values-aligned OScH
- Provide users with high quality, context-appropriate support and feedback
- Provide local technical support and maintenance options
- Increase access to OScH in the regions close to the distributed manufacturers

### Open Science Shop Members
- Create sustainable business by ensuring sufficient amount of work for partners
- The OIA is an informed network which can identify and expand applications in a regionally-appropriate way
- Provide skilled opportunties for hardware engineers
- Prove that openness combined with environmental and economic sustainability is feasible 
- Flexible in-source work to others

### Potential / First Products 

1. Timpan audio dosimeter
2. Reflectometer
3. Thermocylclers (PocketPCR Kit: https://gaudishop.ch/index.php/product/pocketpcr-kit/)
4. Gel imaging systems (Blue and white light Transilluminators): Mini LED Transilluminator: https://iorodeo.com/products/mini-led-transilluminator?variant=12386081158;
5. Gel Electrophoresis systems: (Can be optimised? https://www.instructables.com/DIY-Simple-Cheap-Electrophoresis-Setup-for-DNA-Sep/; )
6. qPCR thermocycler (Magnetic Induction Cycler?: https://biomolecularsystems.com/mic-qpcr/
7. Microscopy devices: Open Flexure Microscope? : https://openflexure.org/projects/microscope/
8. Microbiology and CO2 Incubators