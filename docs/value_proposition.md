# Value Proposition

### For Partners
- access to the trademark (open, quality standard that supports designers)
- supports marketing + sales
     - support the trademark (make consumers care!)
     - aggregate demand (go to one website, and we send orders in your zones to you)
     - aggregate forum + discussions to build a sense of community + comfort.
     - honestly 'use of argument' in marketing that we're supporting developers!
     - better technical support (feature development) through access to developers
     - supporting developers! 
- internal process improvements + information sharing
     - Priority is a better work as Exclusivity... Information is accessible to other groups.
     - Institutional Knowledge, sharing best practices
     - Academic or publishing support
     - Mentorship + advisory support
     - maximise information sharing and documentation of know-how for passive access i.e. any developers/manufacturers can read the docs and contribute suggestions, they have access to the time of the group to the same extent as the rest of the border community.
     - limit access to internal review capabilities and specific processes and tools related to quality assurance. For example, the standard operating procedures could be shared with everyone as open know-how but software tools developed to make the quality assurance procedures more robust and the human time for audit and review could be limited to members of the distributed manufacturing network.
- do what you want to do more...
     - Outsource things you don't want to do (dev's want to develop, manufacturers want to manufacture).
- enables greater diversity of developers / manufacturers
     - Manufacturers can be smaller and grow more flexibly
     - Developers can maintain a wider range of projects + interests w/ out large, concentrated investments of time.
     - Share overhead, and therefore reduce overhead / upfront cost for everyone 
- OScH is too complicated for traditional manufacturers
- OScH is too varied and often small-scale (100s of units not M) for traditional manufacturers
- We want something consistent with our goals and values (see GOSH Manifesto and Roadmap)
- OScH needs repair, maintenance and calibration - the people who built it are well placed to provide this so distributed manufacturing should increase % of equipment that is actually working.
- OScH is not a commodity, it has deep value because hardware underpins science, innovation and STEAM education so there are positive feedback loops from increased local availability.

### For Consumers / Purchasers of Equipment
- GOSH-aligned 
     - values-aligned purchasing
     - enables greater diversity of developers and manufacturers
     - access to improved documentation 
     - access to support for customization 
     - motivations to provide add-ons and improvements to get % return
- Not GOSH-aligned
     - price competitive 
     - curation of products that meet "standard"
     - interoperable products (curation of products that are useful together)
     - reduced admin to on-board suppliers in procurement systems (with multiple similar products)
     - more payment methods / more likely to be able to buy in local currency 
     - single point of contact for sales and support (plus correct language)
     - local support for repair / maintenance
     - faster / cheaper delivery
     - locally available (less shipping / customs nonsense)