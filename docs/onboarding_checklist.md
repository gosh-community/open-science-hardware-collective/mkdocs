# Onboarding Checklist

**Once at a place to formally onboard a product, follow this checklist to confirm that you have not forgotten any steps before launch.** 

## Onboarding Steps

_Note, next steps should be brainstorming a complete list with others in the distributed manufacturing subgroup._

 - Has the product been screened to confirm it's a good fit for this distribution structure?
    - _See recommended screening questions [here](https://gosh-community.gitlab.io/open-science-hardware-collective/mkdocs/screening_questions/)._

 - Has the appropriate market analysis been done to determine which manufacturers should produce and/or distribute this product?
    - _Should have a list of places to test different products (groupget, surveys, etc.). This will depend on market._

 - Have developer agreements been signed with the central organization?
    - _Link developer agreement templates once available._

 - Have agreements been written and signed between the developer and the relevant manufacturers?
    - _Link manufacturer agreement templates once available_

 - Has a centralized set of product details, features, and information been circulated with all distributed manufacturers producing this device?
    - _Link product information recommendations / templates once available_

 - Has a branded product page been created in Shopify and deployed on the relevant external sites?
    - _Link Shopify onboarding steps / requirements once available_

 - Have pricing limitations been set with the relevant manufacturers?
 
 - How complex is the product manufacturing and assembly?
     - _Should we determine some way to quantify complexity?_