# Open Science Shop Memorandum of Understanding

Document Version: 1.0.0

> _tl:dr in italicized block quotes - these are just notes, not part of the actual doc!_

## A. Introduction and Vision

This Memorandum of Understanding (MOU) is entered into by the parties listed in Appendix A (herein referred to as "Partner" and collectively the "Partners"). Each Partner agrees to be bound by the terms of this MOU from the date of their respective signature entered in Appendix B (each such date, an "Effective Date").

> This 'multi-party' agreement setupsets us up to allow people to sign off on this one by one (separate page per signature)... no need to 'resign' whenever a new member is added.

WHEREAS, **Partners have Shared Values** outlined by the Gathering for Open Science Hardware (herein "GOSH") Manifesto, available here: https://openhardware.science/gosh-manifesto/.

WHEREAS, **Partners have a Shared Goal** of making Open Science Hardware (herein "OScH") the standard for all scientific hardware manufacturing and sales.

WHEREAS, **Partners want a Special Collaboration** among developers, manufacturers, marketers, distributors and key supply chain actors and believe it is _required_ to achieve this goal

WHEREAS, **Partners have unique situations, skills, and talents** to contribute to this goal.

NOW, THEREFORE, the **Partners agree to the following Shared Intent**:

## B. Shared Intent

### 1. Overview

Through our Shared Values and Goals, we intend to enter into a Special Collaboration based on each Partners unique situation, skills, and talents.  The intent of this Special Collaboration is to support the expansion of Partners' existing OScH and speed up the development and distribution of future OScH projects.  This MOU represents a **baseline level of agreement among Partners** which enables partners to collaborative easier, faster, and with less friction.  When combined with additional agreements, like a **Product Agreeement** or **Service Agreement**, Partners can have clear, contractual pathways to engage with each other in a well defined and mutually beneficial way.

### 2. Roles and Responsibilities

**The Partners agree to:**

1. Faithfully and honestly pursue one or more parts of our **Common Mission and Values** as outlined in Appendix A.
2. Sign, or intend to sign in the near future, a **Service Agreement** to define the way(s) they can support our Common Mission.
3. Make special efforts to **cooperate with Partners**
4. Make special efforts to **support the Open Science Shop** and our collective endeavors.

> _Let's explain these parts.  (1) means you agree on the mission.  (2) means you're going to say exactly how you'll contribute.  (3) and (4) basically mean that you are going to take this seriously and (as much as possible) you'll collaborative, promote, and support Parters and the OSS.  The specific aren't defined because they differ for each person, but it's a broad way to agree to 'take it seriously'._

**The Partners shall receive:**

1. A personal or organizational listing on the Open Science Shop website and opportunities for promotion through Open Science Shop communications.
2. The ability to list products on the Open Science Shop website and opportunities for promotion through Open Scienc Shop communciations.

> _Needs more work, but basically we'll promote you and your stuff.  We could include access to a more limited Element channel for discussions or other things... dunno we'll have to see._

## C. Terms of Service

### 1. Dispute Resolution Process

In the event of a dispute arising from or in connection with this MOU or any subsequent Product or Service Agreements, the Partners shall first seek to resolve the dispute through direct, good faith negotiations. If the dispute cannot be resolved within a period of thirty (30) days, the Partners agree to engage in mediation with a mutually agreed-upon mediator. If mediation fails to resolve the dispute, the Partners may pursue any other remedies available to them under the applicable law. The costs of mediation shall be shared equally among the involved Parties.

> _Partners will mediate their own problems... if not they'll find someone to help mediate, if not then you can pursue legal recourse (like suing or whatever)._

### 2. Termination of Partnership

Any Partner may terminate their participation in this MOU upon thirty (30) days' written notice to the other Partners. Upon termination, the terminating Partner shall fulfill any outstanding obligations prior to the effective date of termination. Termination of this MOU by any Partner shall not affect the validity or duration of any existing Product or Service Agreements between the remaining Partners and the terminating Partner, which shall continue to be governed by their respective terms unless otherwise agreed upon in writing.

> _Partners can terminate with 30 days notice.  Termination of the MOU doesn't affect Product / Service agreements._

### 3. Confidentiality and Intellectual Property

While the intent of the Open Science Shop is to create open source, transparent and publicly available technology, we recognize that there may be times when confidentiality or intellectual property may be a consideration.  Partners engaged through this MOU or any subsequent Product or Service Agreements should clearly communicate needs and expectations around confidential information or intellectual property _prior to engage in a collaboration_.  If needed, both parties agree to negotiate in good faith and _establish separate confidentiality and intellectual property agreements as required_ for any sensitive information or intellectual property that may be shared, developed, or accessed as a result of this MOU or any subsequent Service or Product Agreement. Such agreements should clearly define the scope of confidentiality, the handling of intellectual property, ownership rights, and the duties of both parties regarding the non-disclosure and protection of confidential information and intellectual property.

> _We don't want to touch IP or Confidentiality conflicts... so this pushes that back to bilateral agreements between Partners._

### 4. Amendments

This MOU may be amended only by a written agreement signed by all Partners. Amendments to individual Product or Service Agreements shall not constitute amendments to this MOU, and shall only require the signatures of the Parties to the respective agreement.

> _To change the MOU, all parties have to re-sign_

## Appendix A: Common Mission and Values

### Mission

1. _Make OScH available_ to **consumers** anywhere in the world. 
2. Provide **manufacturers** _access to high quality designs and markets_ to sell them in.
3. Provide **developers** with _fast, scalable manufacturing and distributions channels_.
4. Match **skilled members** with _projects that need them_.
5. _Align incentives and de-risk collaboration_ by standardizing relationships between **all community partners**.

### Values

1. **We are built on GOSH values** and follow all values defined in the [GOSH Manifesto](http://openhardware.science/gosh-manifesto/), however, some clauses are put into the context of this MOU below.
    - **OScH is accessible**: We will increase access to OScH in the regions close to the distributed manufacturers
    - **OScH makes science better**: We will provide users with a consistent, comparable, high quality product & high quality, context-appropriate support and feedback
    - **OScH is impactful tools**: We will strive to create sustainable businesses by ensuring a sufficient amount of work for partners
    - **OScH empowers people**: We will help match skilled Partners with opportunities to use those skills towards the distribution of OScH.
    - **OScH has no black boxes**: We want to have more OScH with better access to local technical support and maintenance options
    - **OScH allows multiple futures for science**: We hope to show an alternative to traditional, IP-focused distribution models and that openness combined with environmental and economic sustainability is feasible.
2. **We put people first** and address technology and cognitive justice
3. **We learn from experiments** together, so one persons failure can be many people's lesson, rather than vice versa.
4. **We're honest** and willing to admit if things are not working out and move forward
5. **We create a collaborative environment** - Direct competition in open source groups is certainly possible and has some upsides, but if we're actively collaborating it can make collaboration harder. Like many distributors, I think we establish zones where we provide rights to partners to sell a product using the combined brand / trademark, etc. This enables better collaboration and reduces points of friction.
    - create boiler-plate but OScH appropriate product-based contracts to simplify collaboration through the development -> manufacturing -> distribution -> sales process.
        - establish 'sales zones' for manufacturing partners with the agreements
    - create boiler-plate memorandum of understanding and service agreements to enable anyone to engage the community through beneficial relationships to them and the community partners.
6. **We value contributors** - We do not have IP, there is only 1 point of value exchange (sale of the product)... so we don't have traditional ways to incentivize / empower the developer and manufacturer separately. We need to transparently and actively provide funding back to developers, and use that as a point of marketing and education to the end user.
    - ensure our product-based contracts carve out options for % of sales returned to all participating parties, or other options based on common needs.
7. **We share** - Manufacturers also innovate heavily. We should share those innovation and ensure we have toolchains which enable manufacturer-led innovation (versioning, forking, multiple options for assembly or parts lists, collaborative process for SOP updating, etc.).
    - ensure in agreements that 'know-how' is shared (no IP), we share this knowhow not only within a product chain, but throughout the broader community.
8. **We're compelling, we do not compel** - Anyone will be able to see and replicate what we do... our only point of 'control' will be a trademarked quality spec. The cost should be low and value of engagement should be high.
    - Have a simple, consistent Memorandum of Understanding for anyone to join - this keeps our 'common' agreement a clear but low bar.
    - Layer clear service and product agreements which specify the way a member wants to interact.  This allows every member to specify how they want to engage and contribute to our common goal.
9. **We seek sustainable, long-term partnerships** - People will pop up who want to make one thing, or a few things, for a short period... even for new, good partners, there is a cost to the group to onboard, train, and support a new partners. For not so good partners, that cost can be extremely high. We need a process to identify + learn about potential partners that is efficient and effective. We probably also need a process for allowing existing partnerships to die out if they are no longer productive or aligned.
    - legal docs have 'onboarding' clauses and 'release' clauses for partners.

## Appendix B: Signatory Partners

_Starred items are required._

Prospective Partners wishing to join the MOU or any connected agreements must receive the co-signature of at least 51% of existing Partners, up to a maximum of 6. This requirement ensures that Prospective Partners are aligned with the shared values and mission of the Open Science Shop. The initial two founding members are exempt from this requirement and shall be considered founding Partners of the MOU.

Existing Partners who co-sign for a Prospective Partner must participate in the Prospective Partner Onboarding process, the details of which are outlined in a separate document.

----------- page break ---------------

**Partner's Acknowledgment:**

The undersigned hereby affirms that they are duly authorized to represent the Partner and to enter into this MOU.  If the Partner is an organization, complete the Organization and Title sections.  If the Partner is an indiviaul, complete only the Name section.  By signing below, They affirm that they have been duly onboarded by the co-signing Partners and agree to contribute to the Open Science Shop's common mission and values.

* Name*:
* Organization:
* Title:
* Zip code*:
* Signature*:
* Date*:

**Co-signing Partners Acknowledgment:**

The co-signing Partner hereby affirms that they have participated in the Prospective Partner Onboarding process with the Prospective Partner and they endorse and support their addition to this document.

Each co-signing Partner must provide the following information:

* Name*:
* Organization:
* Title:
* Signature*:
* Date*:

[Additional lines for co-signatures as required]