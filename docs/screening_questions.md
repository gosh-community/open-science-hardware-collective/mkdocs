# Screening Questions

**Before formally onboarding a new product for distributed manufacturing, there should be extensive conversations with the developers. This will help to ensure that the product is at the appropriate stage and holds a broad enough market for this distributed structure to be useful.** 

## Initial Screening Questions

_Note, next steps should be brainstorming a complete list with others in the distributed manufacturing subgroup._

 - What's the market size?

   - How does the market size distribute globally?

 - What are the developers' expectations for this product and distribution structure?

 - What pricing minimums and maximums should be set for this product?
 
 - How complex is the product manufacturing and assembly?

 - Are you going to resell or manufacture locally?

 - Do you have manufacturing documentation or tutorials? (how easy will it be to take on in other locations?)

 - Do you have packaging for this device?

 - Do you have marketing materials? Or do you need some of our resources for this?
