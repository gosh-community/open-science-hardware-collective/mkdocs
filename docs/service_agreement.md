# Open Science Shop Service Agreement

Document Version: 1.0.0
Referenced MOU Document Version: 1.0.0

## A. Introduction

This Service Agreement ("Agreement") is a supplementary document to the Open Science Shop Memorandum of Understanding ("MOU") and is entered into by the undersigned Partner (the "Service Provider") and the other Partners of the Open Science Shop MOU (collectively, the "Service Recipients").  The specific Partners engaged in receiving or providing services are referred to as "Parties" or, individually, "Party".

The Service Provider agrees to provide services to the Service Recipients under the terms and conditions set forth in this Agreement. The scope and terms of the services are specified herein and are in line with the shared values and goals outlined in the MOU.

## B. Services

The Service Provider hereby agrees to provide the following services:

### 1. Discounted Products/Services:

The Service Provider, [Partner A], agrees to make available services at a discounted rate of $XX/hr to other Partners.

Services shall include but are not limited to [list specific engineering services or refer to an appendix or separate document detailing the services].

### 2. Pro Bono Products/Services:

The Service Provider, [Partner A], agrees to provide up to X hours per month of free [specify type] services to Partners seeking help.

These services are further described as follows [provide details or reference an additional document].

### 3. Pro Bono Communication/Promotion:

The Service Provider, [Partner A], agrees to provide X unique promotional opportunities to interested Partners.

These services are further described as follows [provide details or reference an additional document].

### 4. Other Custom Services:

Additional services offered by the Service Provider shall be outlined in Appendix B, attached hereto and incorporated herein by reference.

## C. Terms of Service

### 1. Term and Termination

This Agreement shall commence on the Effective Date and shall continue in effect until terminated by the Service Provider with a 10 day notice to the other Party.

### 2. Independent Contractor

The Service Provider is engaged as an independent contractor. Nothing herein shall be deemed to create an employer-employee relationship between the Parties.

### 3. Confidentiality and Intellectual Property

Service Providers and Service Recipients should clearly communicate needs and expectations around confidential information or intellectual property.  If confidential information or intellectual property are involved, both parties agree to negotiate in good faith and establish separate confidentiality and intellectual property agreements as required for any sensitive information or intellectual property that may be shared, developed, or accessed as a result of this MOU or any subsequent Service or Product Agreement. Such agreements should clearly define the scope of confidentiality, the handling of intellectual property, ownership rights, and the duties of both parties regarding the non-disclosure and protection of confidential information and intellectual property.

### 4. Amendments

This Agreement may only be amended in writing, signed by both Parties.

## Appendix A: Signatory Partners

_Starred items are required._

Parties wishing to join the MOU or any connected agreements must receive the co-signature of at least 51% of existing Partners, up to a maximum of 6. This requirement ensures that Parties are aligned with the shared values and mission of the Open Science Shop. The initial two founding members are exempt from this requirement and shall be considered founding Partners of the MOU.

Existing Partners who co-sign for a New Partner must participate in the Service Agreement Onboarding process, the details of which are outlined in a separate document.

----------- page break ---------------

**Service Provider's Acknowledgment:**

The undersigned hereby affirms that they are duly authorized to represent the Service Provider and to enter into this MOU.  If the Service Provider is an organization, complete the Organization and Title sections.  If the Service Provider is an indiviaul, complete only the Name section.  By signing below, They affirm that they have been duly onboarded by the co-signing Partners and agree to contribute to the Open Science Shop's common mission and values.

* Name*:
* Organization:
* Title:
* Zip code*:
* Signature*:
* Date*:

**Co-signing Partners Acknowledgment:**

The co-signing Partner hereby affirms that they have participated in the Service Agreement Onboarding process with the Service Provider and they endorse and support the addition of the Service Provider to this document.

Each co-signing Partner must provide the following information:

* Name*:
* Organization:
* Title:
* Signature*:
* Date*:

[Additional lines for co-signatures as required]