# Shipping Policy 

**Obviously, we still need to write this one out (Shopify does not have a template like the other pages)**

#### What we should note:
- include that we will replace lost / stolen items
- notify buyers that we will not cover duties / import taxes (they should expect these charges later)
  - for internaitonal shipping, we should use UPS, FedEx, DHL (likely the best option) -- these are best at getting things through borders
    - DO NOT use USPS
    - we can provide support if we need to / if something gets stuck
    - also the whole point of this distributed manufacturing is to avoid shipping issues

[Here](https://www.shopify.com/blog/shipping-policy#two) are some recommendations from Shopify for drafting shipping policies