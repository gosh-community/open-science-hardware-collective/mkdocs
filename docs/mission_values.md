# Mission & Values

### Vision

We want to achieve OScH ubiquity in the science hardware marketplace. Networking has helped, but is not getting us the last mile.  We need to build an intentional, collaborative community focused on streamlining designing, manacturing and selling OScH products.  We believe the people, the talent, the products, and the will all exists - we just need to get organized!

### Mission

1. _Make OScH available_ to **consumers** anywhere in the world. 
2. Provide **manufacturers** _access to high quality designs and markets_ to sell them in.
3. Provide **developers** with _fast, scalable manufacturing and distributions channels_.
4. Match **skilled members** with _projects that need them_.
5. _Align incentives and de-risk collaboration_ by standardizing relationships between **all community partners**.

### Goals
- Manufacture in a way that supports our shared goals (a la the Manifesto)
- Provide users with a consistent, comparable, high quality product
- Provide users with high quality, context-appropriate support + feedback
- Create sustainable business by ensuring sufficient amount of work for partners
- Provide skilled opportunities for hardware engineers
- Provide local technical support and maintenance options
- Increase access to OScH in the regions close to the distributed manufacturers
- Proving that openness combined with environmental and economic sustainability is feasible

### Values
*  **We are built on GOSH values** from --> [GOSH Manifesto](http://openhardware.science/gosh-manifesto/) --> "Accessible", "Changes the culture of science", "Empowers People"...
    - **OScH is accessible**: Increase access to OScH in the regions close to the distributed manufacturers 
    ← how do we populate what we actually do inside these ‘values bucket’...
    - **OScH makes science better**: Provide users with a consistent, comparable, high quality product & high quality, context-appropriate support and feedback
    - **OScH is impactful tools**: Create sustainable business by ensuring a sufficient amount of work for partners
    - **OScH empowers people: Provide skilled opportunities for hardware engineers
    - **OScH has no black boxes**: Provide local technical support and maintenance options
    - **OScH allows multiple futures for science**: Proving that openness combined with environmental and economic sustainability is feasible
* **We put people first** and address technology and cognitive justice
* **We learn from experiments** together, so one persons failure can be many people's lesson, rather than vice versa.
* **We're honest** and willing to admit if things are not working out and move forward
* **We create a collaborative environment** - Direct competition in open source groups is certainly possible and has some upsides, but if we're actively collaborating it can make collaboration harder. Like many distributors, I think we establish zones where we provide rights to partners to sell a product using the combined brand / trademark, etc. This enables better collaboration and reduces points of friction.
    - create boiler-plate but OScH appropriate product-based contracts to simplify collaboration through the development -> manufacturing -> distribution -> sales process.
        - establish 'sales zones' for manufacturing partners with the agreements
    - create boiler-plate memorandum of understanding and service agreements to enable anyone to engage the community through beneficial relationships to them and the community partners.
* **We value contributors** - We do not have IP, there is only 1 point of value exchange (sale of the product)... so we don't have traditional ways to incentivize / empower the developer and manufacturer separately. We need to transparently and actively provide funding back to developers, and use that as a point of marketing and education to the end user.
    - ensure our product-based contracts carve out options for % of sales returned to all participating parties, or other options based on common needs.
* **We share** - Manufacturers also innovate heavily. We should share those innovation and ensure we have toolchains which enable manufacturer-led innovation (versioning, forking, multiple options for assembly or parts lists, collaborative process for SOP updating, etc.).
    - ensure in agreements that 'know-how' is shared (no IP), we share this knowhow not only within a product chain, but throughout the broader community.
* **We're compelling, we do not compel** - Anyone will be able to see and replicate what we do... our only point of 'control' will be a trademarked quality spec. The cost should be low and value of engagement should be high.
    - Have a simple, consistent Memorandum of Understanding for anyone to join - this keeps our 'common' agreement a clear but low bar.
    - Layer clear service and product agreements which specify the way a member wants to interact.  This allows every member to specify how they want to engage and contribute to our common goal.
* **We seek sustainable, long-term partnerships** - People will pop up who want to make one thing, or a few things, for a short period... even for new, good partners, there is a cost to the group to onboard, train, and support a new partners. For not so good partners, that cost can be extremely high. We need a process to identify + learn about potential partners that is efficient and effective. We probably also need a process for allowing existing partnerships to die out if they are no longer productive or aligned.
    - legal docs have 'onboarding' clauses and 'release' clauses for partners.