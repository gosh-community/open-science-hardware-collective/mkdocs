# Consolidated Governance Notes / Ideas

## Cooperative Concept:

Cooperative comprising of two groups:

1. Developers of tech
2. Manufacturers of tech

Developers define a product agreement for each product which primarily defines their % for sales of that products.

Within each product, there are distributions agreements which each manufacturer based on zone.  This defines the % of sales to manufacturer and who is responsible for what in the supply chain for that manufacturing - sales channel.

Patronage (ie if the coop produces profits, how those profits are distributed) is based on use of the coop (for producers - total sales of their product lines, for manufacturers - total value of manufactured products).

Voting is 1 member : 1 vote.

## Basic Structure from first-pass distribution agreement

We have a governance structure such that manufacturers and developers, on a collective-wide and on a product by product basis, can agree to the specifications of the collaboration.

#### We have a ‘collective-wide’ ‘constitution’ that:
- Described what toolchains we use
- Outline how we controls + provide the QA / QC trademark
- Has a mechanism for members of the collective to flag if issue arises with regional manufacturer (e.g. they aren’t following toolchain, violating QA/QC trademark, etc) and determine remedial action.
- Details decision making for collectively pushing out new versions of a manufacturing package including QA/QC systems and support, mapped to hardware versions released by the developer
- Identifies and tracks against our plan for economic and environmental sustainability (per value above)
- Provides values based equity assessment and plan for improvement-- per values above-- what additional training could be useful for developers, engineers, etc.
- Explains how we are ensuring that reqional labor is treated fairly-- for instance we don’t inadvertently promote poor labor practices-- this should be protected for.

#### The ‘collective’ (maybe it’s an org, or a person assigned to the task… I dunno) is responsible for:
- Centralized marketing
- Maintaining centralized website with forum, tech support, etc.
- Onboarding / offboarding partners
- Maintaining legal paperwork (agreements w/ partners)
- Centralized payments (??) (payments from buyers to manufacturers, separating + sending cut to developer…)
- Pooling requests and contributions from members to get additional improvements to the hardware e.g. that aren’t on the developer’s roadmap and they would not do otherwise.
- Helping navigate local regulations, necessary insurances (e.g. product liability), external testing and certification that might be required (e.g. EMC). Doing these things remains the responsibility of each manufacturer, although there might be options to pool resources e.g. insurance across a specific region.
- Maintaining any software tools to help with QA/QC or private sharing and support between members.

#### We have a product-by-product ‘constitution’ that says
- Who is the developer(s) of this product?
- What % of sales do they get (remuneration scheme)?
- What % of sales does the manufacturer get?
- What % of sales goes back to the co-op/collective (overhead of this). Perhaps this is a cut of the developers % or the mark-up is split in three portions, though they don’t need to be equal (probably easier to manage this way). Generally 10% is a standard FS fee, but I personally think this is too low to operate on. To really build out this financial structure we need to do an assessment of basic operating costs of a co-op if that’s the direction this is heading.
- What spec’s must be hit to assign the quality mark on this product?
- How are those spec’s identified and put in place (manufacturers + developers (?))?
- What is the sales zone for this product?


### Drawing by Julian

![image](https://gitlab.com/gosh-community/open-science-hardware-collective/project-management-stuff/uploads/0d170c028c31801e2d9362838c38930f/GOSH-Collective.jpg)

## Conversations with individuals who have experience with cooperatives

Greg reached out to neighbor who has experience, this was his  response:

"I can advise on co-op tax issues in the US and the experience I have with some US co-ops with international members."

"The basics are that a US co-op can have international member owners and that does not by itself tie them into filing US taxes.  However if that US co-op issues patronage dividends that does require the international members to pay US taxes on that income.  However if those international are already connected to the US and filing taxes here that may not matter."

"I do not know the tax aspects of co-ops incorporated in other countries but my guess is you would be better off organized in some other country that does not tax international owners of businesses organized there.
If you do want to look further into US co-ops my consulting rate is $300 per hour.  Likely the above will be enough for now but let me know."

## Conversations with Matt Bower (lawyer)

**About cooperatives in the US**

A cooperative isn't an entity in the face of the state like a limited liability corporation, or a c-corp, or a partnership, etc.  A cooperative is just a way to set up an organization (governance).  It can be a non or for profit and can be a range of actual organization types in the face of the state.

Most cooperative lawyers in the US are familiar with housing cooperatives or trade cooperatives... this is something that's unique because it's a mix of a producer cooperative and marketing cooperative... so likely we'll need to create some new structures.

**Setting this up as a 501c3 (tax exempt) non profit in the US**

- tax exemption would not allow members to have a stake in the business, so you cannot have a model in which beneficiaries (devs and manufacturers) have governance roles (are on the board)
- it's possible to be a non-profit, but can't be tax exempt.

**Can we set this up inside GOSH?**

- Well, yes, but then none of the members (devs, manufacturers) would or even could have any role in governance (couldn't be on the board, etc.).
- It may also be a bit more complex in getting additional services (finalizing a product, marketing, etc.), to ensure that there's no conflict of interests.
- This feels like a no go, but I suppose if dev's and manufacturers are OK without any governance in the process, then maybe it's ok (?)

**What about setting it up as a subsidiary of GOSH**

- Basically this only adds complexity.  A subsidiary is build to eliminate double taxation of money flowing _up_ from the subsidiary to the parent... in our case, that's not the goal.
- It wouldn't help the distributed manufacturing group raise funding through the non-profit... it would actually probably only make it harder.

**So a for- or non-profit cooperatives using c-corp or llc is probably right**

- The only thing is it can't be a 501c3 because we want members to also have governance.

**If it's a cooperative, what country should we make it in?**

- We chose the US because we already had people here, and because when moving money around, the dollar is a really convenient common currency (transactions to a from).  
- However... other countries actually have cooperatives as a legal structure, perhaps with additional rights or benefits (?)... it's possible there are enough benefits to that to make it compelling to work another place.

**General questions about structure**

- we've discussed these a lot, but here's a list of things to consider...
- is there a board?
- is there an ED?
- what are the rights / obligations membership
- what are the rights and benefits of membership
- what are the membership dues
- goals and mission of company
- what are the terms of those agreement
- how are we going to fund this to get started?

**Next steps**

- [ ] Greg will see if I can talk to this cooperative guy in Ann Arbor... he's worked on a wide range of diverse cooperatives and may have some good examples or provide some interesting answers to how to structure this.
- [ ] Meet again before GOSH panama to prepare to present and discuss this idea.
