# Welcome to the GOSH Community Distributed Manufacturing Templates & Procedure Page

**The goal of this site is to create a space to house all standard procedures and templates / formats for the GOSH Community distributed manufacturing structure.**

 Note, this site is meant to be used as an internal resource to create a level of standardization across all distributed manufacturing operations. The purpose of this platform is to document everything we need for the manufacturers, developers, and operations people within the distributed manufacturing group. We will likely also need a public-facing platform to link legal pages and disclaimers for individual products. 

 To view example products for the distributed structure, visit the Our Sci Reflectometer Product Page [here](https://www.our-sci.net/reflectometer/). As this page develops, you will start to see completed templates of various legal policies housed on this site.