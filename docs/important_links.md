# Important Links

- [Pricing Analysis](https://docs.google.com/spreadsheets/d/1p9ADTHurYWXg4AJlBqYD0JccrpxkOBzuHdevA-7OvTw/edit#gid=963463690)
- [Drafted Agreements](https://docs.google.com/document/d/1OiIr6pKd8Q6M0sWWXX_FDuGFRM7zmBsiiTOmdgGkbNw/edit#heading=h.hude0gmz880v)
- [Structure Visualization](https://docs.google.com/drawings/d/1edVaduF8KgAbeA5hUqzRc9qZUA8Vf4jvFW7ZBrnUOzs/edit)
- [Miro Board](https://miro.com/app/board/o9J_lGABhnw=/)
- [Website](https://openinstrumdev.wpengine.com/)