### Documentation template

If you use this template, you can create a static HTML webpage of whatever documentation you save in here.  It creates a nice indexed structure like this https://www.mkdocs.org/user-guide/writing-your-docs/.

### Using this templates in Our Sci

To use this template in your group, you need to:

1. To customize for this use, we changed two lines in mkdocs.yml file in the main folder
   - site_name: GOSH Distributed Manufacturing Templates & Procedures
   - site_url: https://gosh-community.gitlab.io/open-science-hardware-collective/mkdocs/ 

You can add markdown files to your repository and the index will match the folder structure of your files!

### Learn more about mkdocs generally 

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.
